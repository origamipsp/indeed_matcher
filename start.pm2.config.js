// Запуск этого приложения: pm2 start site.config.js

module.exports = {
    apps: [{
        name: "indeed",
        script: "./app.js",
        env: {
            NODE_ENV: "production",
            WORKING: 0,
            PORT: 5001
        },
        env_dev: {
            NODE_ENV: "production",
            WORKING: 0,
            PORT: 5001
        },
        // instances : "2",
        // exec_mode : "cluster"
        out_file: "/dev/null",
        error_file: "/dev/null"
    }]
}