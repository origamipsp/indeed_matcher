module.exports = (sequelize, Sequelize) => {
    const Vacancy = sequelize.define("table_vacancies", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },

        companyId: {
            type: Sequelize.INTEGER,
        },

        title: {
            type: Sequelize.STRING
        },
        url: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.TEXT()
        },

        location: {
            type: Sequelize.STRING(500)
        },

        city: {
            type: Sequelize.STRING(500)
        },

        logo: {
            type: Sequelize.STRING(2000)
        },

        companyName: {
            type: Sequelize.STRING(2000)
        },

        companyUrl: {
            type: Sequelize.STRING(2000)
        },




    },
        {
            indexes: [
                // Create a unique index on url
                {
                    unique: true,
                    fields: ['url']
                }
            ]
        })

    return Vacancy
}