module.exports = (sequelize, Sequelize) => {
    const Exception = sequelize.define("table_exceptions", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        exception: {
            type: Sequelize.STRING
        },
    })

    return Exception
}