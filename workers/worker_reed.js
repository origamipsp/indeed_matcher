const { parentPort } = require('worker_threads')
const axios = require('axios-https-proxy-fix')
const cheerio = require('cheerio')
const db = require('../models/index.js')
const { Op } = require("sequelize");
const c = require('ansi-colors');
const log = require('simple-node-logger')
const { green } = require('ansi-colors')
const logger = log.createSimpleFileLogger('project.log')

const Vacancy = db.vacancy
const Company = db.company
const Scammer = db.scammer
const min = 100
const max = 195
const matchCount = 4
//const locations = ['DE', 'FR', 'FI', 'SE', 'IE', 'HK', 'NL', 'SG', 'GB', 'CA', 'AT', 'ES', 'CZ', 'AE']
const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// получаем количество страниц потенциальных скамеров, удовлетворяющих поиску по тайтлу искомой вакансии заказчика
async function getPagesCountParent(url) {
  await delay(getRandomInt(min, max))
  try {
    //const html = await axios.get(`${url}-jobs`)
    const html = await axios.get(`${url}`)
    const $ = cheerio.load(html.data)
    let jobsCount = ""
    $('div.row.search-results > div.col-sm-8.col-md-9.results-container > div.search-results-footer > div:nth-child(1) > div.row-top.hidden-xs > div > div > div').each((i, elem) => {
      jobsCount += `${$(elem).text()}`
    })
    jobsCount = jobsCount.replace(/\s/g, '');
    let start = jobsCount.indexOf("f")
    let end = jobsCount.indexOf("j")
    jobsCount = jobsCount.slice(start + 1, end)
    jobsCount = parseInt(jobsCount)
    let pagesCount = Math.ceil(jobsCount / 25)
    return pagesCount

  } catch (error) {
    console.log(`getPagesCount error reed: ${error.message}`)
  }
}

async function doAll({ url, id, cmpName, location, exceptions }) {
  console.log(location)
  console.log(c.green("Worker is working..."))
  let idList = []
  let pageCount = await getPagesCountParent(url)
  for (let i = 0; i < pageCount; i++) {
    idList = idList.concat(await getUrlsFromPage(url, i,))
    console.log("VACANCY TOTAL: ", idList.length)
  }
  let arr = [...new Set(idList)]
  console.log(c.green(`UNIQ VACANCY TOTAL: ${arr.length}`))
  console.log(arr)
  await parseAll(arr, id, location, exceptions, cmpName, url) // сдесь передаем все url вакансий, всех компаний переданных в post запросе
}

async function getUrlsFromPage(agency, i) {
  let idList = []
  // do {
  //await delay(getRandomInt(min, max))
  try {
    console.log("PARSING PAGE №: ", i + 1)
    const html = await axios.get(`${agency}?pageno=${i}`)
    const $ = cheerio.load(html.data)
    $('div > div.col-sm-12.col-md-9.details > header > h3 > a').each((i, elem) => {
      let vacancyId = `${$(elem).attr('href')}`
      idList.push(vacancyId)
    })
    console.log("VACANCY COUNT PARSED ON PAGE №: ", i + 1, " IS: ", idList.length)
  }
  catch (e) {
    console.log(c.red("getUrlsFromPage error reed:", e))
    logger.error(`getUrlsFromPage error reed:  ${e}`)
  }
  // } while (idList.length == 0);
  console.log("VACANCY FROM PAGE: ", idList.length)
  //console.log("UNIQ VACANCY FROM PAGE: " ,[...new Set(idList)].length)
  return idList
}


//парсим всё нужное нам содержимое из каждой url массива
async function parseAll(idList, companyId, location, exceptions, cmpName, cmpUrl) {

  for (let i = 0; i < idList.length; i++) {

    try {
      const html = await axios.get(`https://www.reed.co.${location}${idList[i]}`)
      const $ = cheerio.load(html.data)

      let title = ""
      $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > h1').each((i, elem) => {
        title += `${$(elem).text()}`
      })

      let titleForParsing // это идёт в парсинг
      try {
        titleForParsing = title.replace(/[^a-zа-яё0-9-\s]/gi, '').split(' ').filter(Boolean).join('%20') // тонкости языка, тире заменено на %20, тайтл заменен на titleForParsing
      } catch (error) {
        titleForParsing = "" // тайтл заменен на titleForParsing
        console.log("title error reed", error)
        logger.error(`title error reed:  ${error}`)
      }

      let description = ""
      $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > div.description-container.row > div > div.description > span').each((i, elem) => {
        description += `${$(elem).text()}`
      })

      let allLi = []
      let uls = []
      try {
        $(`div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > div.description-container.row > div > div.description > span > ul`).each((i, elem) => {
          let arrOfLi = []
          $("li", elem).each((j, elem) => {
            let li = `${$(elem).text()}`.trim()
            arrOfLi.push(li)
            allLi.push(li)
          })
          uls.push(arrOfLi)
        })
      } catch (error) {
        console.log("1-error reed:", error)
      }

      if (uls.length == 0) {
        for (let i = 3; i < 10; i++) {
          try {
            let p = ""
            $(`div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > div.description-container.row > div > div.description > span > p:nth-child(${i})`).each((i, elem) => {
              p += `${$(elem).text()}`
              if (p.length > 120 && p.length < 150) {
                uls.push(p)
              } else if (p.length > 150) {
                p = p.substring(0, 150).split(' ').filter(Boolean)
                let removingLastWord = p.splice(-1, 1);
                p = p.join(' ')
                uls.push(p)
              }
            })
          } catch (error) {
            console.log("2- reed", error)
          }
        }
      }

      let companyName = ""
      $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > div > span > a > span').each((i, elem) => {
        companyName += `${$(elem).text()}`
      })

      let companyUrl = ""
      $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > div > span > a').each((i, elem) => {
        companyUrl += `${$(elem).attr('href')}`
      })

      let city = ""
      $('div.hidden-xs > div > div.location.col-xs-12.col-sm-6.col-md-6.col-lg-6 > span:nth-child(3) > a > span > span').each((i, elem) => {
        city += `${$(elem).text()}`

      })

      let logo = ""
      $('.logo-profile.hidden-xs.text-center > a.logo-wrap.logo-wrap--border-bottom > img').each((i, elem) => {
        logo += `${$(elem).attr('src')}`
      })

      const [vacancy, created2] = await Vacancy.findOrCreate({
        where: {
          url: "" + idList[i]
        },
        defaults: { companyId, title, url: `https://www.reed.co.${location}${idList[i]}`, description, location, city, logo, companyName: cmpName, companyUrl: cmpUrl }
      });

      console.log(c.green("VACANCY SAVED"))
      await parseJobsUrl({
        id: vacancy.id, title: titleForParsing, url: idList[i],   // тайтл заменен на titleForParsing
        description: description,
        city: city, logo: logo,
        // companyName: companyName,
        // companyUrl: companyUrl,
        cmpName: cmpName,
        cmpUrl: cmpUrl
      }, location, uls, allLi, exceptions)
    } catch (e) {
      console.log(c.red(`VACANCY NOT SAVED error reed: ${e}`))
      logger.error(`VACANCY NOT SAVED error reed:  ${e}`)
    }
  }
}


// получаем количество страниц потенциальных скамеров, удовлетворяющих поиску по тайтлу искомой вакансии заказчика
async function getPagesCount(title, location) {
  await delay(getRandomInt(min, max))
  try {
    const html = await axios.get(`https://www.reed.co.${location}/jobs/${title}-jobs`)
    const $ = cheerio.load(html.data)
    let jobsCount = ""
    $('div.row.search-results > div.col-sm-8.col-md-9.results-container > div.search-results-footer > div:nth-child(1) > div.row-top.hidden-xs > div > div > div').each((i, elem) => {
      jobsCount += `${$(elem).text()}`
    })
    jobsCount = jobsCount.replace(/\s/g, '');
    let start = jobsCount.indexOf("f")
    let end = jobsCount.indexOf("j")
    jobsCount = jobsCount.slice(start + 1, end)
    jobsCount = parseInt(jobsCount)
    //console.log(jobsCount)
    let pagesCount = Math.ceil(jobsCount / 25)
    //console.log(pagesCount)
    return pagesCount
  } catch (error) {
    console.log(`getPagesCount error reed: ${error.message}`)
  }
}


function chunkArray(myArray, chunk_size) {
  let results = [];
  while (myArray.length) {
    results.push(myArray.splice(0, chunk_size));
  }
  return results;
}


//парсим url всех потенциальных скамеров, закидываем их в массив  obj = 1 вакансия
async function parseJobsUrl(obj, location, uls, allLi, exceptions) {
  let potentialScammerList = []
  let queries = []
  let url = `https://${location}.indeed.com/jobs?q=`
  if (uls.length) {
    uls.forEach(ul => {
      // получаю списки ul = отдельный список / li
      // берём список, чанкаем его, по 5 и через энкод ури запихиваем в запрос по 5 елементов
      chunkArray(ul, 5).forEach(chunk => {
        queries.push(url + encodeURI(chunk.join("")))
      })
    });

    try {
      for (let j = 0; j < queries.length; j++) {
        try {
          console.log(queries[j])
          const html = await axios.get(`${queries[j]}`, {
            headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' },
            proxy: { host: '77.83.1.112', port: 7951, auth: { username: 'igp3020744', password: 'Ly63R3mjv8' } }
          })
          const $ = cheerio.load(html.data)

          $('div > div > div > div.slider_item.css-kyg8or.eu4oa1w0 > div > table.jobCard_mainContent.big6_visualChanges > tbody > tr > td > div.css-1xpvg2o.e37uo190 > h2 > a').each((i, elem) => {

            let vacancyId = `${$(elem).attr('href')}`
            let cutId = vacancyId.substring(vacancyId.indexOf('=') + 1, vacancyId.indexOf('&'));
            if (cutId.length > 16) {
              cutId = cutId.substring(cutId.lastIndexOf('-') + 1, cutId.lastIndexOf('?'))
            }
            potentialScammerList.push(cutId)
          })
          console.log(c.blue(`POTENTIAL SCAMER LIST IS: ${potentialScammerList.length}`))
        } catch (error) {
          logger.error(`parseJobsUrl title IN CYCLE reed: ${obj.title}, error: ${error}`)
          console.log(c.red(`ParseJobsUrl error reed: ${error}`))
        }
      }
    } catch (error) {
      console.log(c.red(`Out of page range error reed: ${error}`))
      logger.error(`parseJobsUrl OUT OF CYCLE error reed: ${error}`)
    }
    await parseAllFrompotentialScammer([...new Set(potentialScammerList)], obj, url, location, allLi, exceptions)
  }
}


//парсим всё нужное нам содержимое  из каждой url массива потенциальных скамеров
async function parseAllFrompotentialScammer(urls, obj, url, location, allLi, exceptions) {

  console.log(urls)
  for (let i = 0; i < urls.length; i++) {
    try {
      //await delay(getRandomInt(min, max + 350))
      const html = await axios.get(`https://${location}.indeed.com/viewjob?jk=${urls[i]}`, {
        headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' },
        proxy: { host: '77.83.1.112', port: 7951, auth: { username: 'igp3020744', password: 'Ly63R3mjv8' } }
      })
      const $ = cheerio.load(html.data)

      let title = ""
      $('div.jobsearch-JobInfoHeader-title-container > h1').each((i, elem) => {
        title += `${$(elem).text()}`
      })

      console.log(c.yellow(`TITLE:  ${title}`))

      let description = ""
      $('#jobDescriptionText').each((i, elem) => {
        description += `${$(elem).text()}`
      })

      let companyName = ""
      $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
        companyName += `${$(elem).text()}`
      })
      if (companyName == "")
        $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div').each((i, elem) => {
          companyName += `${$(elem).text()}`
        })

      console.log(c.yellow(`COMPANY NAME: ${companyName}`))

      let companyUrl = ""
      $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
        companyUrl += `${$(elem).attr('href')}`
      })
      console.log(c.yellow(`COMPANY URL: ${companyUrl}`))

      let city = ""
      $('.jobsearch-CompanyInfoContainer > div > div > div > div:nth-child(2) > div').each((i, elem) => {
        city += `${$(elem).text()}`
      })
      console.log(c.yellow(`CITY: ${city}`))

      let logo = ""
      $('.jobsearch-ViewJobLayout-companyPromo > div > div > div.icl-Card-body > a > img').each((i, elem) => {
        logo += `${$(elem).attr('src')}`
      })
      console.log(c.yellow(`LOGO: ${logo}`))

      let liCountMatch = 0
      let liMatch = []
      for (let li = 0; li < allLi.length; li++) {
        if (description.toLowerCase().includes(allLi[li].toLowerCase()) && !exceptions.includes(allLi[li].toLowerCase())) {
          liCountMatch++
          liMatch.push(allLi[li])
        }
      }
      if (liCountMatch >= matchCount) {
        console.log(c.red('SCAMMER!!!!!!!!!!!!!!!!!!!!!!!!!!'));
        const [scammer, created] = await Scammer.findOrCreate({
          where: {
            [Op.or]: [
              { url: `https://${location}.indeed.com/viewjob?jk=` + urls[i] },
              { companyName: companyName }
            ]
          },
          defaults: { vacancyId: obj.id, url: `https://${location}.indeed.com/viewjob?jk=` + urls[i], companyName, jobTitle: title, description, location: location == "www" ? "US" : location, city, logo, query: url }
        });

        if (process.env.NODE_ENV === 'production') {
          try {
            const post = await axios.post('https://matcher2.bubbleapps.io/api/1.1/wf/matcher_indeed', {
              "source": 'reedcom',
              "clientName": obj.cmpName,
              "clientUrl": obj.cmpUrl,
              "parentVacancyName": obj.title,//
              "parentVacancyUrl": `https://www.reed.co.${location}${obj.url}`,
              "parentVacancyLocation": location,
              "parentVacancyCity": obj.city, //
              "parentCompanyLogo": obj.logo, //
              "query": url, //
              "matchingLines": liMatch,
              "scammerVacancyUrl": urls[i],
              "scammerCompanyName": companyName,
              "scammerJobTitle": title,
              "scammerJobLocation": location,
              "scammerJobCity": city,//
              "scammerCompanyLogo": logo,//
            })
          } catch (error) {
            logger.error(`Error axios.post to https://matcher2.bubbleapps.io/api/1.1/wf/matcher_indeed: error reed: ${e}`)
          }
          try {
            const post = await axios.post('https://hrprime.bubbleapps.io/api/1.1/wf/matcher_indeed', {
              "source": 'reedcom',
              "clientName": obj.cmpName,
              "clientUrl": obj.cmpUrl,
              "parentVacancyName": obj.title,//
              "parentVacancyUrl": `https://www.reed.co.${location}${obj.url}`,
              "parentVacancyLocation": location,
              "parentVacancyCity": obj.city, //
              "parentCompanyLogo": obj.logo, //
              "query": url, //
              "matchingLines": liMatch,
              "scammerVacancyUrl": urls[i],
              "scammerCompanyName": companyName,
              "scammerJobTitle": title,
              "scammerJobLocation": location,
              "scammerJobCity": city,//
              "scammerCompanyLogo": logo,//
            })
          } catch (error) {
            logger.error(`Error axios.post to https://hrprime.bubbleapps.io/version-test/api/1.1/wf/matcher_indeed: error reed: ${e}`)
          }
        }
      }
    }
    catch (e) {
      logger.error(`parseAllFrompotentialScammer: url ${urls[i]}, error reed: ${e}`)
      console.log(c.red(`parseAllFrompotentialScammer error reed: ${e} on url ___:${urls[i]}`))
    }
  }
}

// Основной поток передаст нужные вам данные
// через этот прослушиватель событий.
parentPort.on('message', async (param) => {
  const result = await doAll(param);
  // Access the workerData.
  // return the result to main thread.
  parentPort.postMessage("feels good");
});


