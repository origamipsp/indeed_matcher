const { parentPort } = require('worker_threads')
const axios = require('axios')
const cheerio = require('cheerio')
const { distance } = require('fastest-levenshtein')
const db = require('../models/index.js')
const { Op } = require("sequelize");
const c = require('ansi-colors');
const log = require('simple-node-logger')

const logger = log.createSimpleFileLogger('project.log')
const Vacancy = db.vacancy
const Company = db.company
const Scammer = db.scammer
const min = 100
const max = 195
const locations = ['DE', 'FR', 'FI', 'SE', 'IE', 'HK', 'NL', 'SG', 'GB', 'CA', 'AT', 'ES', 'CZ', 'AE']
const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

async function getPagesCountParent(agency) {
  let pagesCount = 0
  let jobsExists = ""
  do {
    try {
      const html = await axios.get(`${agency}/jobs?start=150`)
      const $ = cheerio.load(html.data)
      let jobsCount = ""

      $('.css-18fh2jl.eu4oa1w0 > h2 > span').each((i, elem) => {
        jobsCount += `${$(elem).text()}`
      })

      $('div > div.css-nhxuph.eu4oa1w0 > div > span').each((i, elem) => {
        jobsExists += `${$(elem).text()}`
      })

      if (jobsExists.includes("There are no open jobs")) {
        break
      }
      //jobsCount = jobsCount.replace(/\s/g, ''); // убранно вырезанние пробелов, для локализации, есть тонкости языка
      jobsCount = jobsCount.substring(0, jobsCount.indexOf(' ') + 1) // сабстринг теперь по пробелу, а не букве, для локализации, есть тонкости языка
      jobsCount = jobsCount.replace(",", '');
      jobsCount = parseInt(jobsCount)
      pagesCount = Math.ceil(jobsCount / 150)

    } catch (error) {
      console.log(c.red(`getPagesCountParent error: ${error}`))
      logger.error(`getPagesCountParent error:  ${error}`)
    }
    console.log("TRY TO GET PAGE COUNT IN: ", agency)
    console.log("pagesCount: ", pagesCount)

  } while (!pagesCount || isNaN(pagesCount))

  return pagesCount
}


async function doAll({ url, id, location, companyUrl }) {
  console.log(location)
  console.log(c.green("Worker is working..."))
  let idList = []
  let pageCount = await getPagesCountParent(url)
  for (let i = 0; i < pageCount; i++) {
    idList = idList.concat(await getUrlsFromPage(url, i,))
    console.log("VACANCY TOTAL: ", idList.length)
  }
  let arr = [...new Set(idList)]
  console.log(c.green(`UNIQ VACANCY TOTAL: ${arr.length}`))////////

  Company.update(
    {
      unicJobsCount: arr.length,
      declaredJobsCount: idList.length,
    },
    {
      where: { url: url },
    }
  )





  await parseAll(arr, id, location) // сдесь передаем все url вакансий, всех компаний переданных в post запросе
}


async function getUrlsFromPage(agency, i) {

  let idList = []
  do {
    //await delay(getRandomInt(min, max))
    try {
      console.log("PARSING PAGE №: ", i + 1)
      const html = await axios.get(`${agency}/jobs?start=${i * 150}`)
      const $ = cheerio.load(html.data)
      $('div.css-150v3kv.eu4oa1w0 > ul > li').each((i, elem) => {
        let vacancyId = `${$(elem).attr('data-tn-entityid')}`
        var cutId = vacancyId.substring(vacancyId.indexOf(',') + 1, vacancyId.lastIndexOf(','));
        idList.push(cutId)
      })
      console.log("VACANCY COUNT PARSED ON PAGE №: ", i + 1, " IS: ", idList.length)
    }
    catch (e) {
      //console.log(c.red("getUrlsFromPage error:", e))
      //logger.error(`getUrlsFromPage error:  ${e}`)
    }
  } while (idList.length == 0);
  console.log("VACANCY FROM PAGE: ", idList.length)
  //console.log("UNIQ VACANCY FROM PAGE: " ,[...new Set(idList)].length)
  return idList
}

//парсим всё нужное нам содержимое из каждой url массива
async function parseAll(idList, companyId, location) {
  //https://hk.indeed.com/jobs?q=amazon&l&vjk=6f3cad08c384ed1a
  for (let i = 0; i < idList.length; i++) {
    console.log("PARSING FROM: ", i, " WHERE ID: ", idList[i])

    try {
      const html = await axios.get(`https://www.indeed.com/viewjob?jk=${idList[i]}`)
      const $ = cheerio.load(html.data)

      let title = ""
      if (location == "www") {
        $('.jobsearch-JobInfoHeader-title').each((i, elem) => {
          title += `${$(elem).text()}` // это идёт в бд
        })
      } else if (location != "www") {
        $('.jobsearch-JobInfoHeader-title-container > h1').each((i, elem) => {
          title += `${$(elem).text()}` // это идёт в бд
        })
      }

      //Удаляем мусор из тайтла и приводим его к необходимому виду
      // пример "Delivery Manager/ Lead", в запросе это должно выглядеть так "Delivery-Manager-Lead"
      let titleForParsing // это идёт в парсинг
      try {
        titleForParsing = title.replace(/[^a-zа-яё0-9-\s]/gi, '').split(' ').filter(Boolean).join('%20') // тонкости языка, тире заменено на %20, тайтл заменен на titleForParsing
        //titleForParsing = titleForParsing.split('/').filter(Boolean).join('%2F') // тонкости языка, тире заменено на %20, тайтл заменен на titleForParsing
      } catch (error) {
        titleForParsing = "" // тайтл заменен на titleForParsing
        console.log("title error", error)
        logger.error(`title error:  ${error}`)
      }


      let description = ""
      $('#jobDescriptionText').each((i, elem) => {
        description += `${$(elem).text()}`
      })


      try {
        description = description.split(' ').join('')
        description = description.replace(/\r?\n/g, "")
        if (description.length >= 499) {
          description = description.substring(0, 499)
        }
      } catch (error) {
        description = ""
        console.log("description error", error)
        logger.error(`description error:  ${error}`)
      }


      //////










      
      // let arrForScraping = []

      // let uls = []
      // ulsForLenght = []
      // if (html.data.includes('<ul>')) {
      //   //#jobDescriptionText > ul:nth-child(27)
      //   $('#jobDescriptionText > div > ul').each((_, ul) => uls.push(ul))
      //   uls.forEach(ul => {
      //     $(ul).find('li').each((_, li) => {
      //       console.log($(li).text())
      //       ulsForLenght.push($(li).text())
      //     })
      //     ulsForLenght.push("#$%")
      //   })
      //   console.log("ulsForLenght----", ulsForLenght)
      //   ulsForLenght = ulsForLenght.join(" ").split("#$%") //
      //   console.log("ulsForLenght----splited----", ulsForLenght)
      //   ulsForLenght = ulsForLenght.sort(function (a, b) {
      //     return b.length - a.length;
      //   });
      //   console.log("ulsForLenght----splited----sorted---", ulsForLenght)
      //   console.log("ulsForLenght----splited----sorted---[0]", ulsForLenght[0])
      //   arrForScraping.push(ulsForLenght[0])
      // }




      // if (arrForScraping[0] == "") {
      //   $('#jobDescriptionText > ul').each((_, ul) => uls.push(ul))
      //   uls.forEach(ul => {
      //     $(ul).find('li').each((_, li) => {
      //       console.log($(li).text())
      //       ulsForLenght.push($(li).text())
      //     })
      //     ulsForLenght.push("#$%")
      //   })
      //   console.log("ulsForLenght----", ulsForLenght)
      //   ulsForLenght = ulsForLenght.join(" ").split("#$%") //
      //   console.log("ulsForLenght----splited----", ulsForLenght)
      //   ulsForLenght = ulsForLenght.sort(function (a, b) {
      //     return b.length - a.length;
      //   });
      //   console.log("ulsForLenght----splited----sorted---", ulsForLenght)
      //   console.log("ulsForLenght----splited----sorted---[0]", ulsForLenght[0])
      //   arrForScraping.push(ulsForLenght[0])
      // }





      if (arrForScraping.length == 0) {
        for (let i = 3; i < 10; i++) {
          try {
            let description = ""
            $(`#jobDescriptionText > div > p:nth-child(${i})`).each((i, elem) => {
              description += `${$(elem).text()}`
              if (description.length > 120 && description.length < 150) {
                arrForScraping.push(description)
              } else if (description.length > 150) {
                description = description.substring(0, 150).split(' ').filter(Boolean)
                let removingLastWord = description.splice(-1, 1);
                description = description.join(' ')
                arrForScraping.push(description)
              }
            })
          } catch (error) {
            console.log("2-", error)
          }
        }
      }








      let fullLength = 0
      let arrForScrapingCut = []

      // if (arrForScraping.length) {
      //   for (let i = 0; i < arrForScraping.length; i++) {
      //     if (fullLength < 1500) {
      //       try {
      //         fullLength = fullLength + arrForScraping[i].length
      //         arrForScrapingCut.push(arrForScraping[i])
      //       } catch (error) {
      //         console.log("3-", error)
      //       }
      //     }
      //   }

      // }


      arrForScrapingCut = arrForScraping.join(" ").substring(0, 300)
      console.log("@@@@@@@@@@@@@@@@@@@@------", arrForScrapingCut)


      try {
        let removingLastWord = arrForScrapingCut.split(" ").splice(-1, 1).join("");
      } catch (error) {
        console.log("splice error: ", error)
      }

      /////// 

      let companyName = ""
      $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
        companyName += `${$(elem).text()}`
      })

      let companyUrl = ""
      $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
        companyUrl += `${$(elem).attr('href')}`
        companyUrl = companyUrl.substring(0, companyUrl.indexOf("?"))
      })

      let city = ""
      $('.jobsearch-CompanyInfoContainer > div > div > div > div:nth-child(2) > div').each((i, elem) => {
        city += `${$(elem).text()}`

      })

      let logo = ""
      $('.jobsearch-ViewJobLayout-companyPromo > div > div > div.icl-Card-body > a > img').each((i, elem) => {
        logo += `${$(elem).attr('src')}`

      })

      const [vacancy, created2] = await Vacancy.findOrCreate({
        where: {
          url: `https://${location}.indeed.com/viewjob?jk=` + idList[i]
        },
        defaults: { companyId, title, url: `https://${location}.indeed.com/viewjob?jk=` + idList[i], description, location: location == "www" ? "US" : location, city, logo, companyName, companyUrl }
      });
      console.log(c.green("VACANCY SAVED"))
      await parseJobsUrl({
        id: vacancy.id, title: titleForParsing, url: idList[i],   // тайтл заменен на titleForParsing
        description: description,
        city: city, logo: logo,
        companyName: companyName,
        companyUrl: companyUrl,
      }, location, arrForScrapingCut)

    } catch (e) {
      console.log(c.red(`VACANCY NOT SAVED error: ${e}`))
      logger.error(`VACANCY NOT SAVED error:  ${e}`)
    }
  }
}

////////////////////////////////////////
async function getPagesCount(title, location, url) {

  let pagesCount;

  try {
    do {
      const html = await axios.get(`${url}&start=0`)
      const $ = cheerio.load(html.data)
      let jobsCount = ""
      $('#searchCountPages').each((i, elem) => {
        jobsCount += `${$(elem).text()}`
      })



      let noResults = ""
      $('div.no_results > div > h1').each((i, elem) => {
        noResults += `${$(elem).text()}`
      })



      jobsCount = jobsCount.replace(/\s/g, '');

      if (jobsCount.includes('|')) { // USA IE HK SG UK CA AE
        let start = jobsCount.indexOf("f")
        let end = jobsCount.length
        jobsCount = jobsCount.slice(start + 1, end)
        jobsCount = jobsCount.replace(/[a-zа-яё]/gi, '');
        jobsCount = jobsCount.split(',').join('') // добавлено, исправлено получение страниц
      } else if (jobsCount.includes('jobs')) { // USA IE HK SG UK CA AE

        let start = jobsCount.indexOf("f")
        let end = jobsCount.indexOf("j")
        jobsCount = jobsCount.slice(start + 1, end)
        jobsCount = jobsCount.split(',').join('') // добавлено, исправлено получение страниц

      }
      jobsCount = parseInt(jobsCount)

      pagesCount = Math.ceil(jobsCount / 15)

      console.log(c.yellow(`PAGES FIND FOR TITLE: ${title} IS:  ${pagesCount}`))
    } while (isNaN(pagesCount) && noResults.includes("did not match any jobs")); /// ДОБАВЛЕНО, // исправлено && на ||

    return pagesCount >= 10 ? 10 : pagesCount;
  }

  catch (error) {
    //console.log(c.red("getPagesCount error:", error))
    //logger.error(`getPagesCount error:  ${e}`)
  }
}
// #jobDescriptionText > div > ul:nth-child(2)
// #jobDescriptionText > div > ul:nth-child(6)
// #jobDescriptionText > div > ul:nth-child(9)

//парсим url всех потенциальных скамеров, закидываем их в массив  obj = 1 вакансия
async function parseJobsUrl(obj, location, arrForScraping) {

  let potentialScammerList = []
  console.log("||||||||||||||", arrForScraping)
  let url = `https://${location}.indeed.com/jobs?q=`
  if (arrForScraping.length) {

    url = url + `${arrForScraping} `
    // for (let i = 0; i < 15; i++) {
    //   url = url + `${arrForScraping[i]} `
    //   // if (i == 15) {
    //   //   break;
    //   // }
    // }
    //url = url.substring(0, url.length - 3).trim()

    console.log("~~~~~|~~~~~", url)


    //  if(url.length > 1900){
    //   url = url.substring(0, 1900)
    //  }



    try {

      let pageCount = await getPagesCount(obj.title, location, url)

      for (let j = 0; j < 1; j++) {

        try {
          // await delay(getRandomInt(min, max))

          const html = await axios.get(`${url}&start=${[j] * 2}0`)
          const $ = cheerio.load(html.data)
          //div.heading4.color-text-primary.singleLineTitle.tapItem-gutter > h2 > a
          //#mosaic-provider-jobcards > ul > li > div > div > div > div.slider_item.css-kyg8or.eu4oa1w0 > div > table.jobCard_mainContent.big6_visualChanges > tbody > tr > td > div.css-1xpvg2o.e37uo190 > h2 > a

          $('div > div > div > div.slider_item.css-kyg8or.eu4oa1w0 > div > table.jobCard_mainContent.big6_visualChanges > tbody > tr > td > div.css-1xpvg2o.e37uo190 > h2 > a').each((i, elem) => {

            let vacancyId = `${$(elem).attr('href')}`
            let cutId = vacancyId.substring(vacancyId.indexOf('=') + 1, vacancyId.indexOf('&'));

            if (cutId.length > 16) {
              cutId = cutId.substring(cutId.lastIndexOf('-') + 1, cutId.lastIndexOf('?'))
            }

            if (obj.url !== cutId) {
              potentialScammerList.push(cutId)
            }
          })

          console.log(c.blue(`POTENTIAL SCAMER LIST IS: ${potentialScammerList.length}`))
        } catch (error) {
          logger.error(`parseJobsUrl title IN CYCLE: ${obj.title}, error: ${error}`)
          console.log(c.red(`ParseJobsUrl error: ${error}`))
        }
      }

    } catch (error) {
      console.log(c.red(`Out of page range error: ${error}`))
      logger.error(`parseJobsUrl OUT OF CYCLE error: ${error}`)
    }

    await parseAllFrompotentialScammer([...new Set(potentialScammerList)], obj, url, location)
  }
}


//парсим всё нужное нам содержимое  из каждой url массива потенциальных скамеров
async function parseAllFrompotentialScammer(urls, obj, url, location) {

  console.log(urls)
  for (let i = 0; i < urls.length; i++) {
    try {
      //await delay(getRandomInt(min, max + 350))
      const html = await axios.get(`https://${location}.indeed.com/viewjob?jk=${urls[i]}`)
      const $ = cheerio.load(html.data)

      let title = ""
      $('div.jobsearch-JobInfoHeader-title-container > h1').each((i, elem) => {
        title += `${$(elem).text()}`
      })
      //Удаляем мусор из тайтла и приводим его к необходимому виду
      // пример "Delivery Manager/ Lead", в запросе это должно выглядеть так "Delivery-Manager-Lead"
      //title = title.replace(/[^a-zа-яё0-9\s]/gi, '').split(' ').filter(Boolean).join('-')
      console.log(c.yellow(`TITLE:  ${title}`))

      let description = ""
      $('#jobDescriptionText').each((i, elem) => {
        description += `${$(elem).text()}`
      })

      description = description.split(' ').join('')
      description = description.replace(/\r?\n/g, "")
      if (description.length >= 499) {
        description = description.substring(0, 499)
      }

      let companyName = ""
      $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
        companyName += `${$(elem).text()}`
      })
      if (companyName == "")
        $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div').each((i, elem) => {
          companyName += `${$(elem).text()}`
        })

      console.log(c.yellow(`COMPANY NAME: ${companyName}`))
      //.jobsearch-CompanyReview--heading
      // .jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a
      // div.jobsearch-InlineCompanyRating.icl-u-xs-mt--xs.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a

      let companyUrl = ""
      $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
        companyUrl += `${$(elem).attr('href')}`
      })
      console.log(c.yellow(`COMPANY URL: ${companyUrl}`))

      let city = ""
      $('.jobsearch-CompanyInfoContainer > div > div > div > div:nth-child(2) > div').each((i, elem) => {
        city += `${$(elem).text()}`
      })
      console.log(c.yellow(`CITY: ${city}`))

      let logo = ""
      $('.jobsearch-ViewJobLayout-companyPromo > div > div > div.icl-Card-body > a > img').each((i, elem) => {
        logo += `${$(elem).attr('src')}`
      })
      console.log(c.yellow(`LOGO: ${logo}`))

      // проверяем расстояние левинштайна между описанием вакансси заказчика и описанием каждого потенциального заказчика
      // если удовлетворяет условию - записываем в бд найденного скамера
      let diff = distance(obj.description, description) / obj.description.length
      //diff < 0.5 && 
      if (companyName.includes(obj.companyName) == false) {
        console.log(c.red('SCAMMER!!!!!!!!!!!!!!!!!!!!!!!!!!'));
        const [scammer, created] = await Scammer.findOrCreate({
          where: {
            //url: urls[i]
            [Op.or]: [
              { url: `https://${location}.indeed.com/viewjob?jk=` + urls[i] },
              { companyName: companyName }
            ]
          },
          defaults: { vacancyId: obj.id, url: `https://${location}.indeed.com/viewjob?jk=` + urls[i], companyName, jobTitle: title, location: location == "www" ? "US" : location, city, logo, query: url }
        });

        if (process.env.NODE_ENV === 'production') {
          try {
            const post = await axios.post('https://matcher2.bubbleapps.io/api/1.1/wf/matcher_indeed', {

              "clientName": obj.companyName,
              "clientUrl": obj.companyUrl,
              "parentVacancyName": obj.title,//
              "parentVacancyUrl": `https://${location}.indeed.com/viewjob?jk=` + obj.url,
              "parentVacancyLocation": location == "www" ? "US" : location,
              "parentVacancyCity": obj.city, //
              "parentCompanyLogo": obj.logo, //

              "query": url, //

              "scammerVacancyUrl": `https://${location}.indeed.com/viewjob?jk=` + urls[i],
              "scammerCompanyName": companyName,
              "scammerJobTitle": title,
              "scammerJobLocation": location == "www" ? "US" : location,
              "scammerJobCity": city,//
              "scammerCompanyLogo": logo,//

            })
          } catch (error) {
            logger.error(`Error axios.post to https://matcher2.bubbleapps.io/api/1.1/wf/matcher_indeed: error: ${e}`)
          }
        }
      }
    }
    catch (e) {
      logger.error(`parseAllFrompotentialScammer: url ${urls[i]}, error: ${e}`)
      console.log(c.red(`parseAllFrompotentialScammer error: ${e} on url ___:${urls[i]}`))
    }
  }
}

// Основной поток передаст нужные вам данные
// через этот прослушиватель событий.
parentPort.on('message', async (param) => {
  const result = await doAll(param);
  // Access the workerData.
  // return the result to main thread.
  parentPort.postMessage(`feels good`);
});
