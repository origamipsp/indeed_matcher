const { StaticPool } = require('node-worker-threads-pool')
const os = require('os')
const db = require('../models/index.js')
const path = require('path')
const axios = require('axios-https-proxy-fix')
const cheerio = require('cheerio')
const { Op } = require("sequelize");
const c = require('ansi-colors');


const Vacancy = db.vacancy
const Company = db.company
const Scammer = db.scammer
const Exception = db.exception

const min = 100
const max = 195
let currentСompany = 0

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

//путь к воркеру
//const filePath = path.join(path.dirname(require.main.filename), 'workers', 'worker_indeed.js');
const indeedWorkerFilePath = path.join(path.dirname(require.main.filename), 'workers', 'worker_indeed.js');
const reedComWorkerFilePath = path.join(path.dirname(require.main.filename), 'workers', 'worker_reed.js');
const numCPUs = os.cpus().length








//получаем количество страниц для парсинга. Только для ридкома.
async function getPagesCount(agency) {
  try {
    const html = await axios.get(`${agency}`, {
      headers: { 'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' }
    })
    const $ = cheerio.load(html.data)
    let jobsCount = ""
    $('div.row.search-results > div.col-sm-8.col-md-9.results-container > div.search-results-footer > div:nth-child(1) > div.row-top.hidden-xs > div > div > div').each((i, elem) => {
      jobsCount += `${$(elem).text()}`
    })
    jobsCount = jobsCount.replace(/\s/g, '');
    let start = jobsCount.indexOf("f")
    let end = jobsCount.indexOf("j")
    jobsCount = jobsCount.slice(start + 1, end)
    jobsCount = parseInt(jobsCount)
    let pagesCount = Math.ceil(jobsCount / 25)
    return pagesCount

  } catch (error) {
    console.log(error)
  }
}






// парсим ТОЛЬКО названия компаний и записываем их в бд - РАБОТАЕТ
async function addCompanies(req, res) {
  console.log(req.body.companies)
  res.status(200).json({ message: 'Working...' })
  let agencyArr = req.body.companies
  //let urlList = []
  for (let j = 0; j < agencyArr.length; j++) {

    if (agencyArr[j].includes("indeed.com")) {
      try { 
        const html = await axios.get(`${agencyArr[j]}`, {
          headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' },
          proxy: { host: "45.155.200.180", port: 8000, auth: { username: 'DsXQUg', password: 'n6bj2x' } }
          
        })
        const $ = cheerio.load(html.data)
        let companyName = ""
        $('.eu4oa1w0 > div.css-1ce69ph.eu4oa1w0 > div.css-12f7u05.e37uo190 > div.css-1e5qoy2.e37uo190 > div > div').each((i, elem) => {
          companyName += `${$(elem).text()}`
        })
        console.log("ADDING COMPANY NAME: ", companyName)
        const [company, created] = await Company.findOrCreate({
          where: {
            url: agencyArr[j]
          },
          defaults: { companyName, url: agencyArr[j] }
        });
      } catch (error) {
        console.log(c.red(`Error in addCompanies: ${agencyArr[j]} error: ${error}`))
      }
    } else {


      //await parseCompanyName(agencyArr[j]) // вынесен парсинг названия компании и запись в бд.
      const pageCount = await getPagesCount(agencyArr[j])
      for (let i = 1; i <= pageCount; i++) {
        try {// https://www.reed.co.uk/jobs/amazon/p2630?pageno=2&sortby=DisplayDate
          const html = await axios.get(`${agencyArr[j]}?pageno=${i}`)
          console.log('add - ', agencyArr[j])
          const $ = cheerio.load(html.data)
          $('div > div.col-sm-12.col-md-9.details > header > h3 > a').each((i, elem) => {
            let cutUrl = `${$(elem).attr('href')}`
            //console.log("!!!!!!!!cutUrl- ", cutUrl)
            let symbol = cutUrl.indexOf("?")
            cutUrl = cutUrl.substring(0, symbol)
            //urlList.push(cutUrl)
          })
          let companyName = ""
          $('div > div.col-sm-12.col-md-9.details > header > div.job-result-heading__posted-by > a').each((i, elem) => {
            companyName = `${$(elem).text()}`
          })
          const [company, created] = await Company.findOrCreate({
            where: { url: agencyArr[j] },
            defaults: { companyName, url: agencyArr[j] }
          });
        }
        catch (e) {
          console.log(c.red(`Error in addCompanies: ${agencyArr[j]} error: ${e}`))
        }
      }
    }
  }
}

async function findScammers(req, res) {
  Vacancy.destroy({ truncate: { cascade: true } });
  Scammer.destroy({ truncate: { cascade: true } });
  //создаем пулл воркеров
  const pool = new StaticPool({
    size: 6,
    task: filePath,
    workerData: 'workerData!'
  });
  if (!+process.env.WORKING) {
    res.status(200).json({ message: 'Started' })
    process.env.WORKING = 1
    const start = new Date().getTime();
    const companies = await Company.findAll()
    let counter = 0;
    for (let i = 0; i < companies.length; i++) {
      (async () => {
        let end = companies[i].url.indexOf('.')
        let location = companies[i].url.slice(8, end)
        const result = await pool.exec({ url: companies[i].url, id: companies[i].id, location: location });
        counter++;
        if (counter >= companies.length - 1) {
          await pool.destroy()
          process.env.WORKING = 0
          const end = new Date().getTime();
          console.log(c.green(`END TIME: ${end - start}ms`));
        }
      })();
    }
  } else if (process.env.WORKING) {
    res.status(200).json({ message: "Working..." })
  }
}




async function findScammersByPhrase(req, res) {

  if (!+process.env.WORKING) {

    let exceptionsArr = []
    const exceptions = await Exception.findAll() //

    Vacancy.destroy({ truncate: { cascade: true } });
    Scammer.destroy({ truncate: { cascade: true } });


    for (let e = 0; e < exceptions.length; e++) {
      exceptionsArr.push(exceptions[e].dataValues.exception.toLowerCase())
    }


    const poolIndeed = new StaticPool({
      size: 6,
      task: indeedWorkerFilePath,
      workerData: 'workerData!'
    });

    const poolReedcom = new StaticPool({
      size: 2,
      task: reedComWorkerFilePath,
      workerData: 'workerData!'
    });

    res.status(200).json({ message: 'Started' })
    process.env.WORKING = 1
    const start = new Date().getTime();
    const companies = await Company.findAll()
    let counter = 0;
    for (let i = 0; i < companies.length; i++) {
      (async () => {



        if (companies[i].url.includes("indeed.com")) {
          let end = companies[i].url.indexOf('.')
          let location = companies[i].url.slice(8, end)
          const indeed = await poolIndeed.exec({ url: companies[i].url, id: companies[i].id, cmpName: companies[i].companyName , location: location, exceptions: exceptionsArr });
        }
        else
          if (companies[i].url.includes("www.reed")) {
            let location = companies[i].url.slice(20, 22)
            const reedcom = await poolReedcom.exec({ url: companies[i].url, id: companies[i].id, cmpName: companies[i].companyName, location: location, exceptions: exceptionsArr });
          }





        currentСompany = i;
        counter++;
        if (counter >= companies.length) {
          await poolIndeed.destroy()
          await poolReedcom.destroy()
          process.env.WORKING = 0
          const end = new Date().getTime();
          console.log(c.green(`END TIME: ${end - start}ms`));
        }
      })();
    }
  } else if (process.env.WORKING) {
    res.status(200).json({ message: "Working..." })
  }
}



async function deleteCompanies(req, res) {
  for (let i = 0; i < req.body.companies.length; i++) {
    try {
      let toDelete = await Company.findOne({
        where: { url: req.body.companies[i] },
      })
      toDelete.destroy()
    } catch (error) {
      console.log(c.red(error));
    }
  }
  res.status(200).json("ALL DONE")
}

//получаем результаты по post запросу в котором передаем url компании и возвращаем массив обьектов
async function getResults(req, res) {

  try {
    const result = await Company.findAll({
      //where: { url: req.body.url }, //TODO
      include: [{
        model: Vacancy,
        include: [{
          model: Scammer,
        }]
      }]
    })
    let results = []
    for (let i = 0; i < result.length; i++) {
      for (let j = 0; j < result[i].dataValues.table_vacancies.length; j++) {
        for (let k = 0; k < result[i].dataValues.table_vacancies[j].dataValues.table_scammers.length; k++) {

          if (typeof result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k] !== 'undefined') {
            results.push(
              {
                "clientName": result[i].dataValues.companyName,
                "clientUrl": result[i].dataValues.url,
                "parentVacancyName": result[i].dataValues.table_vacancies[j].dataValues.title,//
                "parentVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.url,
                "parentVacancyLocation": result[i].dataValues.table_vacancies[j].dataValues.location, //
                "parentVacancyCity": result[i].dataValues.table_vacancies[j].dataValues.city, //
                "parentCompanyLogo": result[i].dataValues.table_vacancies[j].dataValues.logo, //

                "query": result[i].dataValues.table_vacancies[j].dataValues.url, //

                "scammerVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.url,
                "scammerCompanyName": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.companyName,
                "scammerJobTitle": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.jobTitle,
                "scammerJobLocation": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.location,//
                "scammerJobCity": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.city,//
                "scammerCompanyLogo": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.logo,//
              }
            )
          } else {
            results.push(
              {
                "clientName": result[i].dataValues.companyName,
                "clientUrl": result[i].dataValues.url,
                "parentVacancyName": result[i].dataValues.table_vacancies[j].dataValues.title,//
                "parentVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.url,
                "parentVacancyCity": result[i].dataValues.table_vacancies[j].dataValues.city, //
                "parentCompanyLogo": result[i].dataValues.table_vacancies[j].dataValues.logo, //

                "query": result[i].dataValues.table_vacancies[j].dataValues.url, //

                "scammerVacancyUrl": "",
                "scammerCompanyName": "",
                "scammerJobTitle": "",
                "scammerJobCity": "",//
                "scammerCompanyLogo": "",//
              }
            )
          }
        }
      }
    }

    res.status(200).json({ results })
  } catch (error) {
    res.status(500).json({ message: error.message })
  }
}



async function addExceptions(req, res) {
  for (let i = 0; i < req.body.exceptions.length; i++) {
    const [exception, created] = await Exception.findOrCreate({
      where: {
        exception: req.body.exceptions[i]
      },
      defaults: { exception: req.body.exceptions[i] }
    });
  }
  res.status(200).json({ message: 'Done' })
}



async function getProgres(req, res) {
  const companies = await Company.findAll()
  let progres = (currentСompany / companies.length) * 100
  res.status(200).json({ message: progres + "%" })
}


module.exports = {
  addCompanies, findScammers, deleteCompanies, findScammersByPhrase, addExceptions, getProgres, // getResults
}