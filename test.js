const axios = require('axios-https-proxy-fix')
const cheerio = require('cheerio')
const fakeUa = require('fake-useragent');

const c = require('ansi-colors');
const querySelectorAll = require('query-selector');
const jsdom = require("jsdom");
const { BOOLEAN } = require('sequelize');
const { get } = require('config');
const request = require('request');
const requestSSL = require('request-ssl');
const requestCOOKIES = require("request-with-cookies");

const webdriver = require('selenium-webdriver');
const { Driver } = require('selenium-webdriver/chrome');
const hooman = require('hooman');

const proxies = ["216.158.228.233",
  "216.158.228.232", "216.158.228.226", "216.158.228.211", "216.158.228.225", "209.159.154.162", "209.159.154.163", "209.159.154.164", "209.159.154.165", "209.159.154.166", "209.159.154.167",
  "209.159.154.168", "209.159.154.169", "64.20.52.18", "64.20.52.19", "64.20.52.20", "64.20.52.21", "64.20.52.22", "64.20.52.23", "64.20.52.24", "64.20.52.25", "173.214.164.194", "173.214.164.195",
  "173.214.164.196", "173.214.164.197", "173.214.164.198", "173.214.164.199", "173.214.164.200", "173.214.164.201", "69.10.43.4", "69.10.43.7", "69.10.43.11", "69.10.43.12", "104.218.54.115",
  "104.218.54.118", "104.218.54.122", "104.218.54.124", "209.159.155.148", "209.159.155.153", "209.159.155.155", "209.159.155.158", "66.23.238.99", "66.23.238.101", "66.23.238.106", "66.23.238.110",
  "206.72.194.162", "206.72.194.168", "206.72.194.172"]



const userAgents = [
  "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.53 Safari/537.36",
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.53 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.115 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.115 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.115 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.115 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.115 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.115 Safari/537.36",
  "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.115 Safari/537.36",
  "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.115 Safari/537.36",
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.115 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36",
  "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36",
  "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36",
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36",
  "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36",
  "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36",
  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36",
  "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36",
]


// USA IE HK SG UK CA AE
function getProxy(location, num) {
  location
  if (location == "www") {
    return { host: "45.155.200.180", port: 7384, auth: { username: 'iusa3020801', password: 'tpZSbjB43L' } }
  } else
    if (location == "ie") {
      return { host: '188.165.1.148', port: 7951, auth: { username: 'igp3020744', password: 'Ly63R3mjv8' } }
    }

    else
      if (location == "uk") {
        return { host: '77.83.1.112', port: 7951, auth: { username: 'igp3020744', password: 'Ly63R3mjv8' } }
      } else
        if (location == "ca") {
          return { host: '205.204.71.234', port: 7384 }
        }

}


addCompanies("https://www.indeed.com/cmp/Computappoint")
async function addCompanies(agency) {

  try {
    // let Ua = fakeUa()
    // console.log(Ua)
    // // request + proxy + Ua
    // let proxyUrl = "htt://iusa3020817:Rhhusd5pc1@64.20.52.18:7384"
    // let proxiedRequest = request.defaults({ 'proxy': proxyUrl, headers: {
    //   'User-Agent': Ua,
    //   'Cookie': 'ctkgen=1; CTK=1gc6o23mtjkvi801; INDEED_CSRF_TOKEN=dXGEGYOieDLEySJ9PR3R8yZxuUA5YHFL; bvcmpgn=direct; CMP_VISITED=1; cmppmeta="eNoBSAC3/3wqUvdEvyMxCQTRDjvaEHDiI6WRCoL5QLMWygw51gINswKJEaq3NjSB3Ku5oO6vdpje0xLxxCo4OsUkbKwtgf62/5Zy9gGsLHfdIfg="; indeed_rcc=cmppmeta'
    // } });
    // await proxiedRequest.get(agency, function (err, resp, body) {
    //   console.log("RESPONSE FROM REQUEST-", resp.statusCode)
    // })


  
    const cloudscraper = require('cloudscraper-version.two');
    const fs = require('fs');
    
    const webpage = cloudscraper.getting(agency);
    const write = () => {
      return webpage;
    };
    
    console.log(write)

    //request + SSL + proxy + UA
    // requestSSL.addFingerprint(agency, '80:9D:95:68:50:BC:7B:A9:75:B2:A6:DE:CF:98:52:3D:CD:A9:BF:C0');
    // let proxyUrl = "htt://iusa3020817:Rhhusd5pc1@64.20.52.18:7384"
    // let proxiedRequest = requestSSL.defaults({ 'proxy': proxyUrl, headers: Ua });
    // await proxiedRequest.get(agency, function (err, resp, body) {
    //   console.log("RESPONSE FROM REQUEST-", resp.statusCode)
    // })

    // request COOKIES
    // let client = requestCOOKIES.createClient();
    // client(agency, function (error, response, body) {
    //   console.log(response.statusCode) // Prints the google web page.
    // });

    // request({
    //   url: agency,
    //   method: "GET",
    //   header: {
    //     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
    //     'Cookie': 'ctkgen=1; CTK=1gc6o23mtjkvi801; INDEED_CSRF_TOKEN=dXGEGYOieDLEySJ9PR3R8yZxuUA5YHFL; bvcmpgn=direct; CMP_VISITED=1; cmppmeta="eNoBSAC3/3wqUvdEvyMxCQTRDjvaEHDiI6WRCoL5QLMWygw51gINswKJEaq3NjSB3Ku5oO6vdpje0xLxxCo4OsUkbKwtgf62/5Zy9gGsLHfdIfg="; indeed_rcc=cmppmeta'
    //   }
    // })




    // let driver = new webdriver.Builder()
    //     .withCapabilities({'browserName': 'chrome'})
    //     .build();

    // // set the domain
    // driver.get(agency);

    // // set a cookie on the current domain
    // driver.manage().addCookie("test", "cookie-1");

    // // get a page with the cookie
    // driver.get(agency);

    // // read the cookie
    // driver.manage().getCookie('test').then(function (cookie) {
    //    console.log(cookie);
    // });

    // driver.quit();



    // let driver = new webdriver.Builder().
    // usingServer('http://localhost:9515').
    // withCapabilities(webdriver.Capabilities.chrome()).
    // build();
    // // console.log(driver.manage().getCookies()) // Возвращаем список всех файлов cookie
    // driver.get("https://www.google.com/");
    // driver.driver.manage().getCookies()
    // // driver.quit();







    //     const chromeDriver = require('chrome-driver-standalone');
    // console.log(chromeDriver.path);    // path to the embeded chrome driver binary
    // console.log(chromeDriver.version); // X.YY.0 except for version 2.35.1


    // //axios
    // const html = await axios.get(agency, {
    //   headers: { 'User-Agent': Ua },
    //   proxy: { host: "64.20.52.18:7384", port: 7384, auth: { username: 'iusa3020817', password: 'Rhhusd5pc1' } }
    // })

    // const $ = cheerio.load(html)
    // let companyName = ""
    // $('.eu4oa1w0 > div.css-1ce69ph.eu4oa1w0 > div.css-12f7u05.e37uo190 > div.css-1e5qoy2.e37uo190 > div > div').each((i, elem) => {
    //   companyName += `${$(elem).text()}`
    // })

    // console.log("ADDING COMPANY NAME: ", companyName)

  } catch (error) {
    console.log(c.red(`Error in addCompanies: ${agency} error: ${error}`))
  }
}
