const { parentPort } = require('worker_threads')
const axios = require('axios-https-proxy-fix')
const cheerio = require('cheerio')
const db = require('../models/index.js')
const { Op, AggregateError } = require("sequelize");
const c = require('ansi-colors');
const log = require('simple-node-logger')

const logger = log.createSimpleFileLogger('project.log')
const Vacancy = db.vacancy
const Company = db.company
const Scammer = db.scammer
const matchCount = 5
const min = 100
const max = 195
let searchType  //  0-new | 1-old
const locations = ['DE', 'FR', 'FI', 'SE', 'IE', 'HK', 'NL', 'SG', 'GB', 'CA', 'AT', 'ES', 'CZ', 'AE']
// const proxies = ["216.158.228.233",
//   "216.158.228.232", "216.158.228.226", "216.158.228.211", "216.158.228.225", "209.159.154.162", "209.159.154.163", "209.159.154.164", "209.159.154.165", "209.159.154.166", "209.159.154.167",
//   "209.159.154.168", "209.159.154.169", "64.20.52.18", "64.20.52.19", "64.20.52.20", "64.20.52.21", "64.20.52.22", "64.20.52.23", "64.20.52.24", "64.20.52.25", "173.214.164.194", "173.214.164.195",
//   "173.214.164.196", "173.214.164.197", "173.214.164.198", "173.214.164.199", "173.214.164.200", "173.214.164.201", "69.10.43.4", "69.10.43.7", "69.10.43.11", "69.10.43.12", "104.218.54.115",
//   "104.218.54.118", "104.218.54.122", "104.218.54.124", "209.159.155.148", "209.159.155.153", "209.159.155.155", "209.159.155.158", "66.23.238.99", "66.23.238.101", "66.23.238.106", "66.23.238.110",
//   "206.72.194.162", "206.72.194.168", "206.72.194.172"]

const proxies = ["45.155.201.206"]

//const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
// function getRandomInt(min, max) {
//   min = Math.ceil(min);
//   max = Math.floor(max);
//   return Math.floor(Math.random() * (max - min + 1)) + min;
// }

async function getPagesCountParent(agency) {
  let pagesCount = 0
  let jobsExists = ""
  do {
    try {
      const html = await axios.get(`${agency}/jobs?start=150`, {
        headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' }
      })
      const $ = cheerio.load(html.data)
      let jobsCount = ""
      $('.css-18fh2jl.eu4oa1w0 > h2 > span').each((i, elem) => {
        jobsCount += `${$(elem).text()}`
      })
      console.log("pages___", jobsCount)
      $('div > div.css-nhxuph.eu4oa1w0 > div > span').each((i, elem) => {
        jobsExists += `${$(elem).text()}`
      })
      console.log("jobsExists___", jobsExists)
      if (jobsExists.includes("There are no open jobs")) {
        break
      }
      jobsCount = jobsCount.substring(0, jobsCount.indexOf(' ') + 1) // сабстринг теперь по пробелу, а не букве, для локализации, есть тонкости языка
      jobsCount = jobsCount.replace(",", '');
      jobsCount = parseInt(jobsCount)
      pagesCount = Math.ceil(jobsCount / 150)

    } catch (error) {
      console.log(c.red(`getPagesCountParent error OLD: ${error}`))
      logger.error(`getPagesCountParent error OLD:  ${error}`)
    }
    console.log("TRY TO GET PAGE COUNT IN OLD: ", agency)
    console.log("pagesCount OLD: ", pagesCount)
  } while (!pagesCount || isNaN(pagesCount))
  return pagesCount
}


async function getPagesCountParentNew(agency) {
  let pagesCount = 0
  let jobsExists = ""
  do {
    try {
      const html = await axios.get(`https://www.indeed.com/jobs?q=company%3A(${agency})&sort=date&start=10`, {
        headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' },
        proxy: { host: "45.155.200.180", port: 8000, auth: { username: 'DsXQUg', password: 'n6bj2x' } }
      })
      const $ = cheerio.load(html.data)
      let jobsCount = ""
      $('#searchCountPages').each((i, elem) => {
        jobsCount += `${$(elem).text()}`
      })
      $('#resultsCol > div.no_results > div > h1').each((i, elem) => {
        jobsExists += `${$(elem).text()}`
      })
      if (jobsExists.includes("did not match any jobs")) {
        break
      }
      jobsCount = jobsCount.substring(jobsCount.indexOf("f") + 1, jobsCount.indexOf("j") - 1)
      jobsCount = jobsCount.replace(",", '');
      jobsCount = parseInt(jobsCount)
      pagesCount = Math.ceil(jobsCount / 15)
    } catch (error) {
      console.log(c.red(`getPagesCountParentNew error: ${error}`))
      logger.error(`getPagesCountParentNew  error:  ${error}`)
    }
    console.log("TRY TO GET PAGE COUNT New  IN: ", agency)
    console.log("pagesCount New : ", pagesCount)
  } while (!pagesCount || isNaN(pagesCount))
  return pagesCount
}


async function chooseSearchType(cpmName, location) {
  try {
    const html = await axios.get(`https://${location}.indeed.com/jobs?q=company%3A(${cpmName})&sort=date&start=10`, {
      headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' },
      proxy: getProxy(location, 0)
    })
    console.log(c.cyan("NEW search!"))
    searchType = 0
  }
  catch (e) {
    console.log("error in new search--", e)
    if (e.code == 'ERR_BAD_REQUEST') {
      searchType = 1
      console.log(c.cyan("OLD search!"))
    }
  }
}


async function doAll({ url, id, cmpName, location, exceptions }) {


  // console.log("--------",url)
  // console.log("--------",cmpName)



  console.log(location)
  console.log(c.green("Worker is working..."))
  let idList = []
  let cpmName = url.split("/cmp/")
  await chooseSearchType(cpmName[1], location)
  let pageCount = searchType == 0 ? await getPagesCountParentNew(cpmName[1]) : await getPagesCountParent(url)
  for (let i = 0; i < pageCount; i++) {
    idList = idList.concat(await getUrlsFromPage(url, i, location))
    console.log("VACANCY TOTAL: ", idList.length)
  }
  let arr = [...new Set(idList)]
  console.log(c.green(`UNIQ VACANCY TOTAL: ${arr.length}`))
  await parseAll(arr, id, location, exceptions, cmpName, url) // сдесь передаем все url вакансий, всех компаний переданных в post запросе
  Company.update(
    {
      unicJobsCount: arr.length,
      declaredJobsCount: idList.length,
    },
    {
      where: { url: url },
    }
  )
}


// USA IE HK SG UK CA AE
function getProxy(location, num) {
  location
  if (location == "www") {
    return { host: "45.155.200.180", port: 8000, auth: { username: 'DsXQUg', password: 'n6bj2x' } }
  } else
    if (location == "ie") {
      return { host: '188.165.1.148', port: 7951, auth: { username: 'igp3020744', password: 'Ly63R3mjv8' } }
    }
    else
      if (location == "uk") {
        return { host: '77.83.1.112', port: 7951, auth: { username: 'igp3020744', password: 'Ly63R3mjv8' } }
      } else
        if (location == "ca") {
          return { host: '205.204.71.234', port: 7384 }
        }
}



async function getUrlsFromPage(agency, i, location) {
  if (searchType == 0) {
    return await getUrlsFromPageNew(agency, i, location)
  } else if (searchType == 1) {
    return await getUrlsFromPageOld(agency, i)
  }
}

async function getUrlsFromPageNew(agency, i, location) {
  let idList = []
  let proxyNum = 0;
  let tryCount = 0
  do {
    tryCount++
    try {
      console.log("PARSING PAGE NEW №: ", i + 1)
      let cpmName = agency.split("/cmp/")
      const html = await axios.get(`https://www.indeed.com/jobs?q=company%3A(${cpmName[1]})&sort=date&start=${i * 10}`, {
        headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' },
        proxy: getProxy(location, proxyNum)
      })

      const $ = cheerio.load(html.data)
      $('tbody > tr > td > div.css-1xpvg2o.e37uo190 > h2 > a').each((i, elem) => {
        let vacancyId = `${$(elem).attr('data-jk')}`
        idList.push(vacancyId)
        console.log("vacancyId____", vacancyId)
      })
      console.log("VACANCY COUNT PARSED ON PAGE NEW №: ", i + 1, " IS: ", idList.length)
    }
    catch (e) {
      console.log("getUrlsFromPage error indeed NEW:", e)
      logger.error(`getUrlsFromPage error indeed NEW:  ${e}`)
      // try {
      //   if (e.response.status == "400") {//////
      //     proxyNum++
      //   }
      // } catch (error) {
      //   console.log("NO RESPONSE STATUS")
      // }
    }
  } while (idList.length == 0 && tryCount <= proxies.length);
  console.log("VACANCY FROM PAGE: ", idList.length)
  return idList == 0 ? await getUrlsFromPageOld(agency, i) : idList
}

async function getUrlsFromPageOld(agency, i) {
  let idList = []
  do {
    try {
      console.log(agency," PARSING PAGE OLD №: ", i + 1)
      const html = await axios.get(`${agency}/jobs?start=${i * 150}`, {
        headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' },
        proxy: getProxy(location, 0)
      })
      const $ = cheerio.load(html.data)
      $('div.css-150v3kv.eu4oa1w0 > ul > li').each((i, elem) => {
        let vacancyId = `${$(elem).attr('data-tn-entityid')}`
        var cutId = vacancyId.substring(vacancyId.indexOf(',') + 1, vacancyId.lastIndexOf(','));
        idList.push(cutId)
      })
      console.log(agency, "VACANCY COUNT PARSED ON PAGE OLD №: ", i + 1, " IS: ", idList.length)
    }
    catch (e) {
      console.log("getUrlsFromPage error indeed OLD:", e)
      logger.error(`getUrlsFromPage error indeed OLD:  ${e}`)
    }
  } while (idList.length == 0);
  console.log("VACANCY FROM PAGE: ", idList.length)
  return idList
}


//парсим всё нужное нам содержимое из каждой url массива
async function parseAll(idList, companyId, location, exceptions, cmpName, cmpUrl) {

  // console.log("--------parseall ", cmpName)
  // console.log("--------parseall ", cmpUrl)
  for (let i = 0; i < idList.length; i++) {
    console.log("PARSING FROM: ", i, " WHERE ID: ", idList[i])
    try {
      const html = await axios.get(`https://${location}.indeed.com/viewjob?jk=${idList[i]}`, {
        headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' },
        proxy: getProxy(location, 0)
      })
      const $ = cheerio.load(html.data)
      let title = ""
      if (location == "www") {
        $('.jobsearch-JobInfoHeader-title').each((i, elem) => {
          title += `${$(elem).text()}` // это идёт в бд
        })
      } else if (location != "www") {
        $('.jobsearch-JobInfoHeader-title-container > h1').each((i, elem) => {
          title += `${$(elem).text()}` // это идёт в бд
        })
      }
      //Удаляем мусор из тайтла и приводим его к необходимому виду
      // пример "Delivery Manager/ Lead", в запросе это должно выглядеть так "Delivery-Manager-Lead"
      let titleForParsing // это идёт в парсинг
      try {
        titleForParsing = title.replace(/[^a-zа-яё0-9-\s]/gi, '').split(' ').filter(Boolean).join('%20') // тонкости языка, тире заменено на %20, тайтл заменен на titleForParsing
      } catch (error) {
        titleForParsing = "" // тайтл заменен на titleForParsing
        console.log("title error indeed", error)
        logger.error(`title error indeed:  ${error}`)
      }
      let description = ""
      $('#jobDescriptionText').each((i, elem) => {
        description += `${$(elem).text()}`
      })

      let allLi = []
      let uls = []
      try {
        $(`#jobDescriptionText ul`).each((i, elem) => {
          let arrOfLi = []
          $("li", elem).each((j, elem) => {
            let li = `${$(elem).text()}`.trim()
            arrOfLi.push(li)
            allLi.push(li)
          })
          uls.push(arrOfLi)
        })
      } catch (error) {
        console.log("1-error indeed:", error)
      }


      if (uls.length == 0) {
        for (let i = 3; i < 10; i++) {
          try {
            let p = ""
            $(`#jobDescriptionText > div > p:nth-child(${i})`).each((i, elem) => {
              p += `${$(elem).text()}`
              if (p.length > 120 && p.length < 150) {
                uls.push(p)
              } else if (p.length > 150) {
                p = p.substring(0, 150).split(' ').filter(Boolean)
                let removingLastWord = p.splice(-1, 1);
                p = p.join(' ')
                uls.push(p)
              }
            })
          } catch (error) {
            console.log("2- indeed", error)
          }
        }
      }

      // let companyName = ""
      // $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
      //   companyName += `${$(elem).text()}`
      // })

      // let companyUrl = ""
      // $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
      //   companyUrl += `${$(elem).attr('href')}`
      //   companyUrl = companyUrl.substring(0, companyUrl.indexOf("?"))
      // })

      let city = ""
      $('.jobsearch-CompanyInfoContainer > div > div > div > div:nth-child(2) > div').each((i, elem) => {
        city += `${$(elem).text()}`

      })

      let logo = ""
      $('.jobsearch-ViewJobLayout-companyPromo > div > div > div.icl-Card-body > a > img').each((i, elem) => {
        logo += `${$(elem).attr('src')}`

      })

      const [vacancy, created2] = await Vacancy.findOrCreate({
        where: {
          url: `https://${location}.indeed.com/viewjob?jk=` + idList[i]
        },
        defaults: { companyId, title, url: `https://${location}.indeed.com/viewjob?jk=` + idList[i], description, location: location == "www" ? "US" : location, city, logo, companyName: cmpName, companyUrl: cmpUrl }
      });
      console.log(c.green("VACANCY SAVED"))
      await parseJobsUrl({
        id: vacancy.id, title: titleForParsing, url: idList[i],   // тайтл заменен на titleForParsing
        description: description,
        city: city, logo: logo,
        // companyName: companyName,
        // companyUrl: companyUrl,
        cmpName: cmpName,
        cmpUrl: cmpUrl

      }, location, uls, allLi, exceptions)

    } catch (e) {
      console.log(c.red(`VACANCY NOT SAVED error indeed: ${e}`))
      logger.error(`VACANCY NOT SAVED error indeed:  ${e}`)
    }
  }
}


function chunkArray(myArray, chunk_size) {
  let results = [];
  while (myArray.length) {
    results.push(myArray.splice(0, chunk_size));
  }
  return results;
}

//парсим url всех потенциальных скамеров, закидываем их в массив  obj = 1 вакансия
async function parseJobsUrl(obj, location, uls, allLi, exceptions) {

  // console.log("-------parseJobsUrl", obj.cmpName)
  // console.log("-------parseJobsUrl", obj.cmpUrl)


  let potentialScammerList = []
  let queries = []
  let url = `https://${location}.indeed.com/jobs?q=`
  if (uls.length) {
    uls.forEach(ul => {
      // получаю списки ul = отдельный список / li
      // берём список, чанкаем его, по 5 и через энкод ури запихиваем в запрос по 5 елементов
      chunkArray(ul, 5).forEach(chunk => {
        queries.push(url + encodeURI(chunk.join("")))
      })
    });

    try {
      for (let j = 0; j < queries.length; j++) {
        try {
          console.log(queries[j])
          const html = await axios.get(`${queries[j]}`, {
            headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' },
            proxy: getProxy(location, 0)
          })
          const $ = cheerio.load(html.data)

          $('div > div > div > div.slider_item.css-kyg8or.eu4oa1w0 > div > table.jobCard_mainContent.big6_visualChanges > tbody > tr > td > div.css-1xpvg2o.e37uo190 > h2 > a').each((i, elem) => {

            let vacancyId = `${$(elem).attr('href')}`
            let cutId = vacancyId.substring(vacancyId.indexOf('=') + 1, vacancyId.indexOf('&'));
            if (cutId.length > 16) {
              cutId = cutId.substring(cutId.lastIndexOf('-') + 1, cutId.lastIndexOf('?'))
            }
            potentialScammerList.push(cutId)
          })
          console.log(c.blue(`POTENTIAL SCAMER LIST IS: ${potentialScammerList.length}`))

        } catch (error) {
          logger.error(`parseJobsUrl title IN CYCLE: ${obj.title}, error indeed: ${error}`)
          console.log(c.red(`ParseJobsUrl error indeed indeed: ${error}`))
        }
      }
    } catch (error) {
      console.log(c.red(`Out of page range error indeed: ${error}`))
      logger.error(`parseJobsUrl OUT OF CYCLE error indeed: ${error}`)
    }
    await parseAllFrompotentialScammer([...new Set(potentialScammerList)], obj, url, location, allLi, exceptions)
  }
}


//парсим всё нужное нам содержимое  из каждой url массива потенциальных скамеров
async function parseAllFrompotentialScammer(urls, obj, url, location, allLi, exceptions) {

  // console.log("-------parseAllFrompotentialScammer", obj.cmpName)
  // console.log("-------parseAllFrompotentialScammer", obj.cmpUrl)


  console.log(urls)
  for (let i = 0; i < urls.length; i++) {
    try {
      const html = await axios.get(`https://${location}.indeed.com/viewjob?jk=${urls[i]}`, {
        headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' },
        proxy: getProxy(location, 0)
      })
      const $ = cheerio.load(html.data)

      let title = ""
      $('div.jobsearch-JobInfoHeader-title-container > h1').each((i, elem) => {
        title += `${$(elem).text()}`
      })
      console.log(c.yellow(`TITLE:  ${title}`))

      let description = ""
      $('#jobDescriptionText').each((i, elem) => {
        description += `${$(elem).text()}`
      })

      let companyName = ""
      $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
        companyName += `${$(elem).text()}`
      })
      if (companyName == "")
        $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div').each((i, elem) => {
          companyName += `${$(elem).text()}`
        })

      console.log(c.yellow(`COMPANY NAME: ${companyName}`))

      let companyUrl = ""
      $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
        companyUrl += `${$(elem).attr('href')}`
      })
      console.log(c.yellow(`COMPANY URL: ${companyUrl}`))

      let city = ""
      $('.jobsearch-CompanyInfoContainer > div > div > div > div:nth-child(2) > div').each((i, elem) => {
        city += `${$(elem).text()}`
      })
      console.log(c.yellow(`CITY: ${city}`))

      let logo = ""
      $('.jobsearch-ViewJobLayout-companyPromo > div > div > div.icl-Card-body > a > img').each((i, elem) => {
        logo += `${$(elem).attr('src')}`
      })
      console.log(c.yellow(`LOGO: ${logo}`))

      let liCountMatch = 0
      let liMatch = []
      for (let li = 0; li < allLi.length; li++) {
        if (description.toLowerCase().includes(allLi[li].toLowerCase()) && !exceptions.includes(allLi[li].toLowerCase())) {
          liCountMatch++
          liMatch.push(allLi[li])
        }
      }
      if (liCountMatch >= matchCount && !companyName.includes(obj.cmpName)) {
        console.log(c.red('SCAMMER!!!!!!!!!!!!!!!!!!!!!!!!!!'));
        const [scammer, created] = await Scammer.findOrCreate({
          where: {
            [Op.or]: [
              { url: `https://${location}.indeed.com/viewjob?jk=` + urls[i] },
              { companyName: companyName }
            ]
          },
          defaults: { vacancyId: obj.id, url: `https://${location}.indeed.com/viewjob?jk=` + urls[i], companyName, jobTitle: title, description, location: location == "www" ? "US" : location, city, logo, query: url }
        });

        if (process.env.NODE_ENV === 'production') {
          try {
            const post = await axios.post('https://matcher2.bubbleapps.io/api/1.1/wf/matcher_indeed', {
              "source": 'indeed',
              "clientName": obj.cmpName,
              "clientUrl": obj.cmpUrl,
              "parentVacancyName": obj.title,//
              "parentVacancyUrl": `https://${location}.indeed.com/viewjob?jk=` + obj.url,
              "parentVacancyLocation": location == "www" ? "US" : location,
              "parentVacancyCity": obj.city, //
              "parentCompanyLogo": obj.logo, //
              "query": url, //
              "matchingLines": liMatch,
              "scammerVacancyUrl": `https://${location}.indeed.com/viewjob?jk=` + urls[i],
              "scammerCompanyName": companyName,
              "scammerJobTitle": title,
              "scammerJobLocation": location == "www" ? "US" : location,
              "scammerJobCity": city,//
              "scammerCompanyLogo": logo,//
            })
          } catch (error) {
            logger.error(`Error axios.post to https://matcher2.bubbleapps.io/api/1.1/wf/matcher_indeed: error indeed: ${e}`)
          }

          try {
            const post = await axios.post('https://hrprime.bubbleapps.io/api/1.1/wf/matcher_indeed', {
              "source": 'indeed',
              "clientName": obj.cmpName,
              "clientUrl": obj.cmpUrl,
              "parentVacancyName": obj.title,//
              "parentVacancyUrl": `https://${location}.indeed.com/viewjob?jk=` + obj.url,
              "parentVacancyLocation": location == "www" ? "US" : location,
              "parentVacancyCity": obj.city, //
              "parentCompanyLogo": obj.logo, //
              "query": url, //
              "matchingLines": liMatch,
              "scammerVacancyUrl": `https://${location}.indeed.com/viewjob?jk=` + urls[i],
              "scammerCompanyName": companyName,
              "scammerJobTitle": title,
              "scammerJobLocation": location == "www" ? "US" : location,
              "scammerJobCity": city,//
              "scammerCompanyLogo": logo,//
            })
          } catch (error) {
            logger.error(`Error axios.post to https://hrprime.bubbleapps.io/api/1.1/wf/matcher_indeed : error indeed: ${e}`)
          }
        }
      }
    }
    catch (e) {
      logger.error(`parseAllFrompotentialScammer: url ${urls[i]}, error indeed: ${e}`)
      console.log(c.red(`parseAllFrompotentialScammer error indeed: ${e} on url ___:${urls[i]}`))
    }
  }
}

// Основной поток передаст нужные вам данные
// через этот прослушиватель событий.
parentPort.on('message', async (param) => {
  const result = await doAll(param);
  // Access the workerData.
  // return the result to main thread.
  parentPort.postMessage(`feels good`);
});
