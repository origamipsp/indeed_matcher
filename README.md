# indeed_matcher

## API

## addCompanies
- [ ] 1) POST '/api/matcher/addCompanies' *- accepts an array of companies*  
*example:*  

{  

    "companies" : ["Google", ...]  

}  

*Collects data about the company and its vacancies*
___________________________________________________________

## findScammers
- [ ] 2) GET '/api/matcher/findScammers' *- launches a search for scammers*
___________________________________________________________

## getResults
- [ ] 3) POST '/api/matcher/getResults' *- takes the company url, not all companies have their own page, so we pass it like this*  

*example:*  

{  

    "url" : "https://www.indeed.com/cmp/Google"  

}  

*returns result like this*

{  

    "results": [  

        {  
                "clientName": "some customer company name",
                "clientUrl": "customer company url",
                "parentVacancyName": "parent job title",
                "parentVacancyUrl": "parent job url",
                "parentVacancyCity": "parent job location",
                "parentCompanyLogo": "parent job logo",
                "scammerVacancyUrl": "scammer vacancy url",
                "scammerCompanyName": "scammer company name",
                "scammerJobTitle": "scammer job title",
                "scammerJobCity": "scammer job location",
                "scammerCompanyLogo": "scammer job location",

        }  

    ]  

}  

