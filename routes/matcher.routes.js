const { Router } = require('express')
const matcherController = require('../controllers/matcher.controller')
const router = Router()

// '/api/matcher'
//router.post('/addVacancy', matcherController.addVacancies)
router.post('/addCompanies', matcherController.addCompanies)
router.get('/findScammers', matcherController.findScammers)
router.get('/findScammersByPhrase', matcherController.findScammersByPhrase)
//router.get('/getResults', matcherController.getResults)
router.post('/deleteCompanies', matcherController.deleteCompanies)
router.post('/addExceptions', matcherController.addExceptions)
router.get('/getProgres', matcherController.getProgres)

module.exports = router
